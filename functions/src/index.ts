import * as admin from 'firebase-admin';
import * as functions from 'firebase-functions';

import createDummyData from './helpers/createDummyData';

admin.initializeApp(functions.config().firebase);

let lazyDB: FirebaseFirestore.Firestore | null;

export const helloWorld = functions.https.onRequest((request, response) => {
  response.send('Hello from Firebase!');
});

export const getUserApiKey = functions.https.onRequest(async (req, res) => {
  lazyDB = lazyDB || admin.firestore();

  try {
    const uid = req.query.uid;
    if (!uid) {
      throw new Error('uid was not provided in the query');
    }
    const doc = await lazyDB
      .collection('keys')
      .doc(uid)
      .get();

    if (!doc.exists) {
      throw new Error('ApiKey does not exist in the database');
    }
    const { apiKey } = doc.data();

    if (!apiKey) {
      throw new Error('ApiKey is undefined in database');
    }

    res.set('Cache-Control', 'public, max-age=300, s-maxage=600');

    return res.status(200).send(apiKey);
  } catch (err) {
    return res.status(500).send(err.message);
  }
});

export const setupDummyData = functions.https.onRequest(async (req, res) => {
  lazyDB = lazyDB || admin.firestore();

  try {
    const uid = req.query.uid;
    const apikey = req.query.apikey;
    if (!uid) {
      throw new Error('uid was not provided in the query');
    }
    if (!apikey) {
      throw new Error(
        'apikey was not provided in the query, Do not worry about your api key being used on init of this data it is meerly a placeholder and will not be charged',
      );
    }

    await createDummyData(lazyDB, uid, apikey);

    res.set('Cache-Control', 'public, max-age=300, s-maxage=600');

    return res.status(200).send(`Successfully added api key: ${apikey}`);
  } catch (err) {
    return res.status(500).send(err.message);
  }
});
