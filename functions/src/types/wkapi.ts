interface Base {
  object: string | 'user' | 'report' | 'study_material';
  url: string;
  data_updated_at: string;
  pages?: Pages;
  data: UserData | SummaryData | StudyMaterialData;
}

interface Pages {
  per_page: number;
  next_url: string | null;
  previous_url: string | null;
}

interface UserData {
  id: string;
  username: string;
  level: number;
  max_level_granted_by_subscription: number;
  profile_url: string;
  started_at: boolean;
  current_vactation_started_at: string | null;
  subscription: Subscription;
  preferences: UserPreferences;
}

interface Subscription {
  active: boolean;
  type: string | null;
  max_level_granted: number;
  period_ends_at: string | null;
}

interface UserPreferences {
  lessons_autoplay_audio: boolean;
  lessions_batch_size: number;
  lessions_presentation_order: string;
  reviews_autoplay_audio: boolean;
  reviews_display_srs_indicator: boolean;
}

interface SummaryData {
  lessons: SummaryItem[];
  next_reviews_at: string;
  reviews: SummaryItem[];
}

interface SummaryItem {
  available_at: string;
  subject_ids: number[];
}

interface StudyMaterialData {
  created_at: string;
  subject_id: number;
  subject_type: string | 'radical' | 'kanji' | 'vocabulary';
  meaning_note: string | null;
  reading_note: string | null;
  meaning_synonyms: string[];
}

type SRSStagesData = SRSStagesDataItem[];

interface SRSStagesDataItem {
  srs_stage: number | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9;
  srs_stage_name:
    | string
    | 'Initiate'
    | 'Apprentice I'
    | 'Apprentice II'
    | 'Apprentice III'
    | 'Apprentice IV'
    | 'Guru I'
    | 'Guru II'
    | 'Master'
    | 'Enlightened'
    | 'Burned';
  interval:
    | number
    | 0
    | 14440
    | 28800
    | 82800
    | 169200
    | 601200
    | 1206000
    | 2588400
    | 10364400;
  accelerated_interval:
    | number
    | 0
    | 7200
    | 14400
    | 28800
    | 82800
    | 601200
    | 1206000
    | 2588400
    | 10364400;
}

interface;

export { Base, UserData, SummaryData, StudyMaterialData, SRSStagesData };
