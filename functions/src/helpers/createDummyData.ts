export default async (lazyDB, uid, apikey) => {
  // Set test api key
  await lazyDB
    .collection('keys')
    .doc(uid)
    .set({ apiKey: apikey });

  // Set profile data
  await lazyDB
    .collection('profiles')
    .doc(uid)
    .set({
      bannerPhoto:
        'https://cdn-images-1.medium.com/max/1600/1*JIYFJRcPAUtPeHFwx2iuPw.jpeg',
      about: `Hey all, I'm that guy who builds this site, Nice to work with you all皆さん、このサイトを作る人は私です！　よろしくお願いいたします。`,
      classes: ['original', 'test'],
      darkMode: true,
      email: 'drew.business2308@gmail.com',
      photo:
        'https://images.8tracks.com/avatar/i/003/502/371/53602.original-1883.jpg?rect=0,247,1200,1200&q=98&fm=jpg&fit=max&w=1024&h=1024',
      public: true,
      summary: { lessons: 0, reviewsNow: 5, reviewsLater: 0 },
      username: 'Yoroshikun',
    });

  // Set basic data
  await lazyDB
    .collection('summaries')
    .doc(uid)
    .set({
      level: 39,
      kanjiCount: 34,
      kanjiPassed: 4,
      radicalCount: 8,
      radicalPassed: 2,
      vocabCount: 101,
      vocabPassed: 43,
      timestamp: '2018-05-22T11:33:14.183Z',
      lastUpdated: '2018-05-22T11:33:14.183Z',
    });
  await lazyDB
    .collection('summaries')
    .doc(uid)
    .collection('graph')
    .add({
      level: 39,
      kanjiCount: 34,
      kanjiPassed: 2,
      radicalCount: 8,
      radicalPassed: 1,
      vocabCount: 101,
      vocabPassed: 20,
      timestamp: '2018-03-22T11:33:14.183Z',
      lastUpdated: '2018-03-22T11:33:14.183Z',
    });
  await lazyDB
    .collection('summaries')
    .doc(uid)
    .collection('graph')
    .add({
      level: 39,
      kanjiCount: 34,
      kanjiPassed: 0,
      radicalCount: 8,
      radicalPassed: 0,
      vocabCount: 101,
      vocabPassed: 0,
      timestamp: '2018-01-22T11:33:14.183Z',
      lastUpdated: '2018-01-22T11:33:14.183Z',
    });

  // Add Classrooms
  await lazyDB
    .collection('classrooms')
    .doc('original')
    .set({
      [uid]: {
        level: 39,
        kanjiCount: 34,
        kanjiPassed: 0,
        radicalCount: 8,
        radicalPassed: 0,
        vocabCount: 101,
        vocabPassed: 0,
        timestamp: '2018-01-22T11:33:14.183Z',
        lastUpdated: '2018-01-22T11:33:14.183Z',
        username: 'Yoroshi-kun',
      },
    });
  await lazyDB
    .collection('classrooms')
    .doc('test')
    .set({
      [uid]: {
        level: 39,
        kanjiCount: 34,
        kanjiPassed: 0,
        radicalCount: 8,
        radicalPassed: 0,
        vocabCount: 101,
        vocabPassed: 0,
        timestamp: '2018-01-22T11:33:14.183Z',
        lastUpdated: '2018-01-22T11:33:14.183Z',
        username: 'Yoroshi-kun',
      },
    });
};
