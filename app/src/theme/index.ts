import { createGlobalStyle } from 'styled-components';
import styledNormalize from 'styled-normalize';
import { fontSize } from '../constants';
import { Theme } from './types';

const GlobalStyle = createGlobalStyle`
  @import url(${(p: Theme) =>
    `https://fonts.googleapis.com/css?family=${p.fontFamily}:${Object.keys(
      p.fontWeights,
    )
      .map(key => p.fontWeights[key])
      .join(',')}&display=swap`});
  ${styledNormalize};
  ::-webkit-scrollbar-track {
    box-shadow: none;
    background-color: #fff;
  }
  ::-webkit-scrollbar {
    width: 0.25rem;
    background-color: #f0f2f5;
  }
  ::-webkit-scrollbar-thumb {
    border-radius: 500px;
    box-shadow: none;
    background-color: #d1d3dc;
    opacity: 0.5;
  }
  ::-webkit-scrollbar:horizontal {
    height: 0.25rem;
  }
  html {
    font-size: ${fontSize}px;
  }
  body {
    margin: 0;
    padding: 0;
    background-color: ${(p: Theme) => p.palette.layout.bodyBackground};
    color: ${(p: Theme) => p.palette.layout.body};
    font-family: ${(p: Theme) => p.fontFamily}};
    font-weight: ${(p: Theme) => p.fontWeights.normal};
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
    line-height: 1.6;
    overflow: auto;
  }
  body::-webkit-scrollbar {
    width: 0.75rem;
  }
  body::-webkit-scrollbar-track {
    border-left: 1px solid ${(p: Theme) => p.palette.layout.border};
  }
  body::-webkit-scrollbar-thumb {
    border-radius: 0;
  }
  body::-webkit-scrollbar-track:horizontal {
    border-top: 1px solid ${(p: Theme) => p.palette.layout.border};
  }
  body::-webkit-scrollbar:horizontal {
    height: 0.75rem;
    border-left: 0;
  }
  body::-webkit-scrollbar-thumb:horizontal {
    border-radius: 0;
  }
  h1,
  h2,
  h3,
  h4,
  h5,
  h6 {
    color: ${(p: Theme) => p.palette.layout.heading};
    margin-top: 0;
    margin-bottom: 0;
    line-height: 1.4;
    &.normal {
      font-weight: ${(p: Theme) => p.fontWeights.normal};
    }
    &.bold {
      font-weight: ${(p: Theme) => p.fontWeights.semibold};
    }
    &.medium {
      font-weight: ${(p: Theme) => p.fontWeights.medium};
    }
    &.light {
      font-weight: ${(p: Theme) => p.fontWeights.light};
    }
    &.title {
      margin-bottom: 1rem;
    }
    &.subtitle {
      margin-bottom: 2rem;
    }
    &.body {
      color: ${(p: Theme) => p.palette.layout.body};
    }
  }
  strong {
    color: ${(p: Theme) => p.palette.layout.heading};
    font-weight: ${(p: Theme) => p.fontWeights.semibold};
  }
  img {
    max-width: 100%;
  }
  button,
  input,
  optgroup,
  select,
  button {
    background: none;
    border: 0;
    cursor: pointer;
    display: inline-block;
    margin: 0;
    padding: 0;
    &:focus {
      outline: 0;
    }
  }
`;

export default GlobalStyle;
