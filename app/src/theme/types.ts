import { moods } from '../constants/theme';

export interface Fonts {
  [key: string]: string;
  family: string;
  style: string;
  weight: string;
  fileName: string;
}

export type Mood = keyof typeof moods;

export interface PaletteLayout {
  body: string;
  bodyBackground: string;
  heading: string;
  subheading: string;
  border: string;
}

export interface PaletteStatuses {
  success: string;
  alert: string;
  inactive: string;
  active: string;
}

export interface Palette {
  primary: string;
  secondary: string;
  tertiary: string;
  lightGray: string;
  mediumGray: string;
  darkGray: string;
  white: string;
  black: string;
  layout: PaletteLayout;
  statuses: PaletteStatuses;
}

export interface FontWeights {
  [key: string]: number | string;
  light: number | string;
  normal: number | string;
  bold: number | string;
}

export interface Globals {
  boxShadow: string;
  mainMinWidth: string;
  mainMaxWidth: string;
  navigationWidth: string;
  radius: string;
}

export interface Theme {
  mood: Mood;
  palette: Palette;
  fontFamily: string;
  fontWeights: FontWeights;
  globals: Globals;
}
