// Lazy load this like the other?
import axios from 'axios';

const wkapi = (apikey: string) =>
  axios.create({
    baseURL: 'https://api.wanikani.com/v2/',
    headers: { Authorization: `Bearer ${apikey}` },
    method: 'get',
    timeout: 10000,
  });

const getUser = async (apikey: string) => {
  try {
    // Setup the instance
    const Iwkapi = wkapi(apikey);
    // Get user
    const user = await Iwkapi('user');
    // Return the user
    return user;
  } catch (err) {
    return console.error(err.message);
  }
};

// Get your own API Key If somehow you cleared your cache
const getUserAPIKey = async (UID: string, fs: firebase.firestore.Firestore) => {
  try {
    // Query firestore for users APIKey
    const apikeyDoc = await fs
      .collection('keys')
      .doc(UID)
      .get();

    // Get APIKey from firestore
    if (!apikeyDoc.exists) {
      throw Error('Api Key does not exist on the server');
    }

    const data: { apikey?: string } = apikeyDoc.data();

    if (!data.apikey) {
      throw new Error('Api key key does not exist in document');
    }

    return data.apikey;
  } catch (err) {
    return console.error(err.message);
  }
};

export { getUserAPIKey, getUser };
