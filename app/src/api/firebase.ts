// eslint-disable-next-line import/no-unresolved
import config from '../secrets/firebase-config';

const initiateAuth = () => import('firebase/auth');

const initiateFirebase = firebase => {
  if (!firebase.apps.length) {
    firebase.initializeApp(config);
  }
};

const provider = firebase => new firebase.auth.GoogleAuthProvider();

const popSignIn = firebase =>
  firebase.auth().signInWithPopup(provider(firebase));

const fs = firebase => firebase.firestore();

const lazyFirestore = async () => {
  const initiateApp = import('firebase/app');
  const initiateFirestore = import('firebase/firestore');
  const [firebase] = await Promise.all([initiateApp, initiateFirestore]);
  initiateFirebase(firebase);
  return firebase.firestore();
};

export { initiateAuth, initiateFirebase, popSignIn, fs, lazyFirestore };
