import { rem } from 'polished';
import { Theme } from '../theme/types';

export const moods: { dark: 'dark'; light: 'light' } = {
  dark: 'dark',
  light: 'light',
};

const theme: Theme = {
  mood: moods.light,
  palette: {
    primary: '#0294ff',
    secondary: '#00d161',
    tertiary: '#ffca00',
    lightGray: '#daddec',
    mediumGray: '#cacaca',
    darkGray: '#7f8fa4',
    white: '#fefefe',
    black: '#354052',
    layout: {
      body: '#7f8fa4',
      bodyBackground: '#eff3f6',
      heading: '#354052',
      subheading: '#7f8fa4',
      border: '#dfe3e9',
    },
    statuses: {
      success: '#00d161',
      alert: '#f85359',
      inactive: '#c5d0de',
      active: '#0294ff',
    },
  },
  fontFamily: 'Roboto',
  fontWeights: {
    light: 300,
    normal: 400,
    bold: 700,
  },
  globals: {
    boxShadow:
      '0rem 1.5rem 4rem rgba(0,0,0,0.07), 0 1rem 1.75rem rgba(0, 0, 0, 0.02)',
    mainMinWidth: rem(1275),
    mainMaxWidth: rem(1330),
    navigationWidth: rem(250),
    radius: rem(4),
  },
};

const darkTheme: Theme = {
  ...theme,
  mood: moods.dark,
  palette: {
    ...theme.palette,
    lightGray: '#2e3035',
    mediumGray: '#cacaca',
    white: '#36393e',
    black: '#fefefe',
    layout: {
      ...theme.palette.layout,
      body: '#c2c2c2',
      bodyBackground: '#23272a',
      heading: '#fefefe',
      border: '#484b4e',
    },
  },
};

export { theme, darkTheme };
