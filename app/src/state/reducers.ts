import { AnyAction } from 'redux';
import { Action, AppState } from './types';
import { theme } from '../constants/theme';

export const initialState: AppState = {
  theme,
};

export const rootReducer = (
  state = initialState,
  action: Action | AnyAction,
): AppState => {
  switch (action.type) {
    // Theme
    case 'set_theme':
      return { ...state, theme: action.payload };
    default:
      return state;
  }
};
