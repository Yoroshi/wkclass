import React from 'react';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import createStore from './store';

export default ({ element }) => (
  <Provider store={createStore().store}>
    <PersistGate loading={element} persistor={createStore().persistor}>
      {element}
    </PersistGate>
  </Provider>
);
