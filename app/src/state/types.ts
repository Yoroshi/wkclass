import { Theme } from '../theme/types';

export interface AppState {
  theme: Theme | undefined;
}

export interface Action {
  type: 'set_theme';
  payload: Theme;
}
