import React from 'react';
import { Link } from 'gatsby';
import { useSelector } from 'react-redux';

import Layout from '../components/layout';
import Image from '../components/image';
import SEO from '../components/seo';
import Messages from '../components/test-messages';
import { AppState } from '../state/types';

const IndexPage = () => {
  const theme = useSelector((state: AppState) => state.theme);
  return (
    <Layout>
      <SEO title="Home" />
      <h1>Hi people</h1>
      <p>{JSON.stringify(theme)}</p>
      <Messages />
      <p>Welcome to your new Gatsby site.</p>
      <p>Now go build something great.</p>
      <div style={{ maxWidth: `300px`, marginBottom: `1.45rem` }}>
        <Image />
      </div>
      <Link to="/page-2/">Go to page 2</Link>
    </Layout>
  );
};

export default IndexPage;
