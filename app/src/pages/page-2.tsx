import React from 'react';
import { Link } from 'gatsby';

import { useSelector } from 'react-redux';
import Layout from '../components/layout';
import SEO from '../components/seo';
import { AppState } from '../state/types';

const SecondPage = () => {
  const theme = useSelector((state: AppState) => state.theme);
  return (
    <Layout>
      <SEO title="Page two" />
      <h1>Hi from the second page</h1>
      <p>{JSON.stringify(theme)}</p>
      <p>Welcome to page 2</p>
      <Link to="/">Go back to the homepage</Link>
    </Layout>
  );
};

export default SecondPage;
