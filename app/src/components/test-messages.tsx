import React, { useState, useCallback, useEffect } from 'react';
import useFirestore from '../hooks/useFirestore';

const testMessages = () => {
  const fs = useFirestore();
  const [messages, setMessages] = useState();

  const getInitData = useCallback(async () => {
    if (fs) {
      const querySnapshot = await fs.collection('messages').get();
      const messagesArray = [];
      querySnapshot.forEach(doc => {
        messagesArray.push(doc.data());
      });
      setMessages(messagesArray);
    }
  }, [setMessages, fs]);

  useEffect(() => {
    getInitData();
  }, [getInitData]);

  console.log('rendered');

  return <p>{JSON.stringify(messages)}</p>;
};

export default testMessages;
