import React from 'react';
import styled, { keyframes } from 'styled-components';

const FoldCubeAngle = keyframes`
0%,
10% {
  -webkit-transform: perspective(140px) rotateX(-180deg);
  transform: perspective(140px) rotateX(-180deg);
  opacity: 0;
}
25%,
75% {
  -webkit-transform: perspective(140px) rotateX(0deg);
  transform: perspective(140px) rotateX(0deg);
  opacity: 1;
}
90%,
100% {
  -webkit-transform: perspective(140px) rotateY(180deg);
  transform: perspective(140px) rotateY(180deg);
  opacity: 0;
}`;

const FoldingCube = styled.div`
  margin: 20px auto;
  width: 40px;
  height: 40px;
  position: relative;
  -webkit-transform: rotateZ(45deg);
  transform: rotateZ(45deg);
`;

const Cube = styled.div`
  float: left;
  width: 50%;
  height: 50%;
  position: relative;
  -webkit-transform: scale(1.1);
  -ms-transform: scale(1.1);
  transform: scale(1.1);

  ::before {
    content: '';
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    background-color: rgb(225, 100, 255);
    -webkit-animation: ${FoldCubeAngle} 2.4s infinite linear both;
    animation: ${FoldCubeAngle} 2.4s infinite linear both;
    -webkit-transform-origin: 100% 100%;
    -ms-transform-origin: 100% 100%;
    transform-origin: 100% 100%;
  }
`;

const Cube2 = styled(Cube)`
  -webkit-transform: scale(1.1) rotateZ(90deg);
  transform: scale(1.1) rotateZ(90deg);

  ::before {
    -webkit-animation-delay: 0.3s;
    animation-delay: 0.3s;
  }
`;

const Cube4 = styled(Cube)`
  -webkit-transform: scale(1.1) rotateZ(180deg);
  transform: scale(1.1) rotateZ(180deg);

  ::before {
    -webkit-animation-delay: 0.6s;
    animation-delay: 0.6s;
  }
`;
const Cube3 = styled(Cube)`
  -webkit-transform: scale(1.1) rotateZ(270deg);
  transform: scale(1.1) rotateZ(270deg);

  ::before {
    -webkit-animation-delay: 0.9s;
    animation-delay: 0.9s;
  }
`;

const Loader = () => (
  <FoldingCube>
    <Cube />
    <Cube2 />
    <Cube3 />
    <Cube4 />
  </FoldingCube>
);

export default Loader;
