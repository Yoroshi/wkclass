import { useCallback, useEffect, useState } from 'react';

import { lazyFirestore } from '../api/firebase';

const useFirestore = () => {
  const [fs, setFS] = useState<firebase.firestore.Firestore | null>();

  const getFirestore = useCallback(async () => {
    setFS(await lazyFirestore());
  }, [setFS]);

  useEffect(() => {
    getFirestore();
  }, [getFirestore]);

  return fs;
};

export default useFirestore;
